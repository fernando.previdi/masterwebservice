package com.webservice.soap;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.in28minutes.courses.CourseDetails;
import com.in28minutes.courses.DeleteCourseDetailsRequest;
import com.in28minutes.courses.DeleteCourseDetailsResponse;
import com.in28minutes.courses.GetAllCourseDetailsRequest;
import com.in28minutes.courses.GetAllCourseDetailsResponse;
import com.in28minutes.courses.GetCourseDetailsRequest;
import com.in28minutes.courses.GetCourseDetailsResponse;
import com.in28minutes.courses.Status;
import com.webservice.soap.bean.Course;
import com.webservice.soap.service.CourseDetailsService;

@Endpoint
public class CourseDetailsEndpoint {
	
	private static List<Course>courses;
	
	@Autowired
	CourseDetailsService service;
	
	 //method
	//input - GetCourseDetailsRequest
	//output - GetCourseDetailsResponse
	
	//http://in28minutes.com/courses
	//GetCourseDetailsResponse
	@PayloadRoot(namespace = "http://in28minutes.com/courses", localPart = "GetCourseDetailsRequest" )
	@ResponsePayload // converter xml to java
	public GetCourseDetailsResponse processCourseDetailsRequest(@RequestPayload GetCourseDetailsRequest request) {// convert java para xml
		Course course = service.findById(request.getId());
		return mapCourseDetails(course);
	}
	
	@PayloadRoot(namespace = "http://in28minutes.com/courses", localPart = "GetAllCourseDetailsRequest" )
	@ResponsePayload // converter xml to java
	public GetAllCourseDetailsResponse processAllCourseDetailsRequest(@RequestPayload GetAllCourseDetailsRequest request) {// convert java para xml
		List<Course> courses = service.findAll();
		return mapAllCourseDetails(courses);
	}
	
	@PayloadRoot(namespace = "http://in28minutes.com/courses", localPart = "DeleteCourseDetailsRequest" )
	@ResponsePayload // converter xml to java
	public DeleteCourseDetailsResponse deleteAllCourseDetailsRequest(@RequestPayload DeleteCourseDetailsRequest request) {// convert java para xml
		Status status = service.deleteById(request.getId());
		DeleteCourseDetailsResponse response = new DeleteCourseDetailsResponse();
		response.setStatus(status);
		return response;
	}


	private GetCourseDetailsResponse mapCourseDetails(Course course) {
		GetCourseDetailsResponse response = new GetCourseDetailsResponse();
		response.setCourseDetails(mapCourse(course));
		return response;
	}
	
	private GetAllCourseDetailsResponse mapAllCourseDetails(List<Course> courses) {
		GetAllCourseDetailsResponse response = new GetAllCourseDetailsResponse();
		for(Course course: courses) {
			CourseDetails mapCourse = mapCourse(course);
			response.getCourseDetails().add(mapCourse);
		}

		return response;
	}

	private CourseDetails mapCourse(Course course) {
		CourseDetails coursedetails = new CourseDetails();
		coursedetails.setId(course.getId());
		coursedetails.setName(course.getName());
		coursedetails.setDescription(course.getDescription());
		return coursedetails;
	}
	
	
	

}
